from django.contrib.auth import logout, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q, F
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import CreateView, UpdateView, TemplateView, FormView

from .forms import *
from .models.dashboard import Dashboard
from .models.task import TaskRepeatConnection


def foo(request):
    return HttpResponse("Hello world")


def handler404(request, exception):
    return render(request, 'Remember/404.html')


def handler500(request):
    return render(request, 'Remember/500.html')


def handler403(request, exception):
    return render(request, 'Remember/403.html')


def handler400(request, exception):
    return render(request, 'Remember/400.html')


@login_required
def index(request):
    if request.method == 'POST':
        if 'delete' in request.POST:
            Task.objects.get(pk=request.POST['task_id']).delete_actions()
            return HttpResponseRedirect(reverse('Remember:index'))
        if 'edit' in request.POST:
            return HttpResponseRedirect(reverse('Remember:task_edit', args=(request.POST['task_id'],)))

    user = request.user
    tags = request.GET.get('tags', '')
    tags = tags.split(',')
    name = request.GET.get('task_name', '')
    arg_dict = {'tags': request.GET.get('tags', ''),
                'task_name': name}
    # else:
    #     user = request.user
    #     tags = None
    #     name = ''
    #     arg_dict = {}
    if 'finished' in request.GET:
        finished = True
    else:
        finished = False

    tasks = System.get_tasks(user, name, finished, tags).order_by('-priority')
    # paginator = Paginator(tasks, 2)
    # page = request.GET.get('page')
    # if page is None:
    #     page = 0
    # try:
    #     showed_tasks = paginator.page(page)
    # except PageNotAnInteger:
    #     showed_tasks = paginator.page(1)
    # except EmptyPage:
    #     showed_tasks = paginator.page(paginator.num_pages)
    arg_dict['tasks'] = tasks
    return render(request, 'Remember/task_list.html', arg_dict)


@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('Remember:index'))


class SignUpView(FormView):
    template_name = 'Remember/signup.html'
    form_class = SignUpForm

    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return HttpResponseRedirect(reverse('Remember:index'))
        return super().get(*args, **kwargs)

    def form_valid(self, form):
        user = auth_models.User.objects.create(username=form.cleaned_data['username'],
                                               password=form.cleaned_data['password1'],
                                               email=form.cleaned_data['email'])
        user.set_password(form.cleaned_data['password1'])
        user.save()
        login(self.request, user)
        return HttpResponseRedirect(reverse('Remember:index'))


class ProfileView(LoginRequiredMixin, FormView):
    template_name = 'Remember/profile.html'
    form_class = ProfileForm

    def form_valid(self, form):
        user = self.request.user
        user.username = form.cleaned_data['username']
        user.email = form.cleaned_data['email']
        user.set_password(form.cleaned_data['password1'])
        user.save()
        login(self.request, user)
        return HttpResponseRedirect(reverse('Remember:index'))

    def get_initial(self):
        return {
            'username': self.request.user.username,
            'email': self.request.user.email,
            'user': self.request.user
        }


class LoginView(FormView):
    template_name = 'Remember/login.html'
    form_class = LoginForm

    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return HttpResponseRedirect(reverse('Remember:index'))
        return super().get(*args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.cleaned_data['user'])
        return HttpResponseRedirect(reverse('Remember:index'))


class TaskCreateView(LoginRequiredMixin, CreateView):
    form_class = TaskForm
    template_name = 'Remember/task_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_initial(self):
        return {
            'status': consts.MIN_STATUS,
            'name': config.INITIAL_TASK_NAME,
        }

    def form_valid(self, form):
        task = form.save(self.request.user)
        # task = System().add_task(self.request.user, **form.cleaned_data)
        return HttpResponseRedirect(reverse('Remember:task_edit', args=[task.id]))

    def get_success_url(self):
        return reverse('Remember:index')


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    form_class = TaskForm
    template_name = 'Remember/task_update_form.html'

    def __init__(self, *args, **kwargs):
        self.object = None
        super().__init__(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_initial(self):
        initial = super().get_initial()
        initial['tags'] = ', '.join([tag.name for tag in self.get_object().tag_set.all()])
        return initial

    def get(self, request, **kwargs):
        self.object = get_object_or_404(Task, pk=kwargs['task_id'])
        # self.object = System.get_task(request.user, kwargs['task_id'])
        form_class = self.get_form_class()
        form = self.get_form(form_class=form_class)
        context = self.get_context_data(object=self.object, form=form, )
        return self.render_to_response(context)

    def get_object(self, queryset=None):
        obj = get_object_or_404(Task, pk=self.kwargs['task_id'])
        # obj = System.get_task(self.request.user, self.kwargs['task_id'])
        self.check_object_permissions(self.request, obj)
        return obj

    def check_object_permissions(self, request, obj):
        if obj.owner != request.user and obj.assigned != request.user:
            raise PermissionDenied()

    def form_valid(self, form):
        task = form.save(self.request.user)
        task.update_repeats()
        return HttpResponseRedirect(reverse('Remember:task_edit', args=[self.object.id]))

    def get_success_url(self):
        return reverse('Remember:index')


@login_required
def edit_task_complex(request, task_id):
    task = get_object_or_404(Task, pk=task_id)
    if task.owner != request.user:
        raise PermissionDenied()
    if request.method == 'GET':
        if 'connection_add' in request.GET:
            kwargs = {'task_id': task_id}
            if 'user_select' in request.GET:
                kwargs['user_username'] = request.GET['user_select']
            else:
                kwargs['user_username'] = request.user.username
            return HttpResponseRedirect(reverse('Remember:connection_new', kwargs=kwargs))
        if 'sent_connection_edit' in request.GET:
            kwargs = {'task_id': task_id, 'receiver_id': request.GET.get('receiver_id')}
            if 'user_select' in request.POST:
                kwargs['user_username'] = request.GET['user_select']
            else:
                kwargs['user_username'] = request.user.username
            return HttpResponseRedirect(reverse('Remember:connection_edit', kwargs=kwargs))
        if 'parent_add' in request.GET:
            kwargs = {'task_id': task_id}
            if 'user_select' in request.GET:
                kwargs['user_username'] = request.GET['user_select']
            else:
                kwargs['user_username'] = request.user.username
            return HttpResponseRedirect(reverse('Remember:subtask', kwargs=kwargs))
        if 'parent_change' in request.GET:
            kwargs = {'task_id': task_id}
            if 'user_select' in request.GET:
                kwargs['user_username'] = request.GET['user_select']
            else:
                kwargs['user_username'] = request.user.username
            return HttpResponseRedirect(reverse('Remember:subtask_edit', kwargs=kwargs))

    if request.method == 'POST':
        if 'notif_delete' in request.POST:
            System.delete_notification(request.user, request.POST['notification_id'])
        if 'notif_edit' in request.POST:
            return HttpResponseRedirect(reverse('Remember:notification_edit',
                                                kwargs={'task_id': task_id,
                                                        'notification_id': request.POST['notification_id']}))
        if 'repeat_create' in request.POST:
            task = get_object_or_404(Task, pk=task_id)
            if task.end is None:
                return render(request, 'Remember/task_complex_update_form.html',
                              {'task': task,
                               'exclusions': System.get_exclusions_of(request.user, task_id),
                               'repeat_errors': "Can't create repeat for task without timelimit"})
            else:
                return HttpResponseRedirect(reverse('Remember:repeat_new', kwargs={'task_id': task_id}))
        if 'repeat_change' in request.POST:
            return HttpResponseRedirect(reverse('Remember:repeat_edit', kwargs={'task_id': task_id}))
        if 'repeat_remove' in request.POST:
            System.delete_repeat(request.user, task_id)
        if 'exclusion_add' in request.POST:
            return HttpResponseRedirect(reverse('Remember:exclusion_add', kwargs={'task_id': task_id}))
        if 'exclusion_restore' in request.POST:
            System.restore_exclusion(request.user, task_id, int(request.POST['exclusion_index']))
        if 'parent_delete' in request.POST:
            SubtaskConnection.objects.get(sender__id=task_id, receiver__id=int(request.POST['receiver'])).delete()

        # HERE

        if 'subconnection_add' in request.POST:
            kwargs = {'task_id': task_id}
            if 'user_select' in request.POST:
                kwargs['user_username'] = request.POST['user_select']
            else:
                kwargs['user_username'] = request.user.username
            return HttpResponseRedirect(reverse('Remember:subtask', kwargs=kwargs))

        if 'sent_connection_delete' in request.POST:
            TaskConnection.objects.get(sender__id=task_id, receiver__id=request.POST['receiver_id']).delete()

        if 'received_connection_delete' in request.POST:
            TaskConnection.objects.get(sender__id=request.POST['sender_id'], receiver__id=task_id).delete()

    return render(request, 'Remember/task_complex_update_form.html',
                  {'task': get_object_or_404(Task, pk=task_id),
                   'exclusions': System.get_exclusions_of(request.user, task_id)})


@login_required
def task_details(request, task_id):
    task = get_object_or_404(Task, pk=task_id)
    if task.owner != request.user and task.assigned != request.user:
        raise PermissionDenied()
    if request.method == 'POST':
        if 'comment_add' in request.POST:
            System.add_comment(request.user, task_id, request.POST['comment_text'])
            return render(request, 'Remember/task_details.html', {'task': task})
        if 'edit' in request.POST:
            return HttpResponseRedirect(reverse('Remember:task_edit', kwargs={'task_id': task_id}))
        if 'repeat_save' in request.POST:
            Repeat.update_repeat(task_id, request.POST['index'], request.POST['result'], request.POST['status'])
        if 'task_delete' in request.POST:
            task.delete()
            return HttpResponseRedirect(reverse('Remember:index'))
    return render(request, 'Remember/task_details.html', {'task': task})


# Notifications


class NotificationCreateView(LoginRequiredMixin, CreateView):
    form_class = NotificationsForm
    template_name = 'Remember/notification_form.html'

    def get_initial(self):
        return {
            'target': config.NOTIFICATION_DEFAULT_TARGET,
            'time_type': config.NOTIFICATION_DEFAULT_TIME_TYPE,
            'task_id': self.kwargs['task_id']
        }

    def get(self, request, *args, **kwargs):
        task = get_object_or_404(Task, pk=self.kwargs['task_id'])
        if task.owner != self.request.user:
            raise PermissionDenied()
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        task = get_object_or_404(Task, pk=self.kwargs['task_id'])
        notification = form.save(task)
        notification.add_to_repeats()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('Remember:task_edit_complex', args=[self.kwargs['task_id']])


class NotificationUpdateView(LoginRequiredMixin, UpdateView):
    form_class = NotificationsForm
    template_name = 'Remember/notification_form.html'

    def get_initial(self):
        initial = super().get_initial()
        habit_object = self.get_object()
        if habit_object.delta is not None:
            delta = habit_object.delta
            days = delta.days % consts.DAYS_IN_WEEK
            weeks = delta.days // consts.DAYS_IN_WEEK
            hours = delta.seconds // consts.SECONDS_IN_HOUR
            if weeks > NotificationsForm.MAX_TIME_DELTAS:
                days += consts.DAYS_IN_WEEK * (weeks - NotificationsForm.MAX_TIME_DELTAS)
                weeks = NotificationsForm.MAX_TIME_DELTAS
            if days > NotificationsForm.MAX_TIME_DELTAS:
                hours += consts.HOURS_IN_DAY * (days - NotificationsForm.MAX_TIME_DELTAS)
                days = NotificationsForm.MAX_TIME_DELTAS
            initial['hours'] = hours
            initial['weeks'] = weeks
            initial['days'] = days
            initial['datetime'] = None
        initial['task_id'] = self.kwargs['task_id']
        return initial

    def __init__(self, *args, **kwargs):
        self.object = None
        super().__init__(*args, **kwargs)

    def get(self, request, **kwargs):
        self.object = get_object_or_404(Notification, pk=self.kwargs['notification_id'])
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context = self.get_context_data(object=self.object, form=form)
        return self.render_to_response(context)

    def get_object(self, queryset=None):
        obj = get_object_or_404(Notification, pk=self.kwargs['notification_id'])
        self.check_object_permissions(self.request, obj)
        return obj

    def check_object_permissions(self, request, obj):
        if obj.task.owner != request.user:
            raise PermissionDenied()

    def form_valid(self, form):
        form.save(get_object_or_404(Task, pk=self.kwargs['task_id']))
        self.object.update_copies()
        return HttpResponseRedirect(reverse('Remember:task_edit_complex', kwargs={'task_id': self.kwargs['task_id']}))

    def get_success_url(self):
        return reverse('Remember:task_edit_complex')


# REPEAT


class RepeatCreateView(LoginRequiredMixin, CreateView):
    form_class = RepeatForm
    template_name = 'Remember/repeat_form.html'

    def get_initial(self):
        return {'task_id': self.kwargs['task_id']}

    def get(self, request, *args, **kwargs):
        task = get_object_or_404(Task, pk=self.kwargs['task_id'])
        if task.owner != self.request.user:
            raise PermissionDenied()
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        form.cleaned_data.pop('rd_months')
        form.cleaned_data.pop('rd_days')
        form.cleaned_data.pop('rd_weeks')
        System.add_repeat(self.request.user, **form.cleaned_data)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('Remember:task_edit_complex', args=[self.kwargs['task_id']])


class RepeatUpdateView(LoginRequiredMixin, UpdateView):
    form_class = RepeatForm
    template_name = 'Remember/repeat_form.html'

    def get_initial(self):
        initial = super().get_initial()
        habit_object = self.get_object()
        if habit_object.rule_type == Repeat.RELATIVE_DELTA_SHORT:
            days = habit_object.rule_value.days % 7
            weeks = habit_object.rule_value.days // 7
            if weeks > 100:
                days += 7 * (weeks - 100)
                weeks = 100
            initial['rd_weeks'] = weeks
            initial['rd_days'] = days
            initial['rd_months'] = habit_object.rule_value.months + habit_object.rule_value.years * 12
        if habit_object.rule_type == Repeat.I_TH_SHORT:
            initial['i_th_i'], initial['i_th_week_day'] = habit_object.rule_value.split()
        if habit_object.rule_type == Repeat.EVERY_SHORT:
            initial['every_choices'] = habit_object.rule_value
        initial['rule_type'] = habit_object.rule_type
        initial['task_id'] = self.kwargs['task_id']
        return initial

    def __init__(self, *args, **kwargs):
        self.object = None
        super().__init__(*args, **kwargs)

    def get(self, request, **kwargs):
        # TODO parse delta
        self.object = System.get_repeat(request.user, kwargs['task_id'])
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context = self.get_context_data(object=self.object, form=form)
        return self.render_to_response(context)

    def check_object_permissions(self, request, obj):
        if obj.template_task.owner != request.user:
            raise PermissionDenied()

    def get_object(self, queryset=None):
        obj = System.get_repeat(self.request.user, self.kwargs['task_id'])
        self.check_object_permissions(self.request, obj)
        return obj

    def form_valid(self, form):
        form.cleaned_data.pop('rd_months')
        form.cleaned_data.pop('rd_days')
        form.cleaned_data.pop('rd_weeks')
        System.edit_repeat(self.request.user, **form.cleaned_data)
        return HttpResponseRedirect(reverse('Remember:task_edit_complex',
                                            kwargs={'task_id': self.kwargs['task_id']}))

    def get_success_url(self):
        return reverse('Remember:task_edit_complex', args=[self.kwargs['task_id']])


# Exclusion


class ExclusionCreateView(LoginRequiredMixin, CreateView):
    form_class = ExclusionForm
    template_name = 'Remember/exclusion_form.html'

    def get_initial(self):
        task = get_object_or_404(Task, pk=self.kwargs['task_id'])
        # System.get_task(self.request.user, self.kwargs['task_id'])
        return {
            'user': self.request.user,
            'task_id': task.id,
            'status': 0,
            'name': task.name,
            'description': task.description,
            'tags': ', '.join([tag.name for tag in task.tag_set.all()]),
            'priority': task.priority,
            'scope': task.scope
        }

    def get(self, request, *args, **kwargs):
        task = get_object_or_404(Task, pk=self.kwargs['task_id'])
        if task.owner != self.request.user:
            raise PermissionDenied()
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        form.cleaned_data.pop('id_type')
        form.cleaned_data.pop('date')
        form.cleaned_data.pop('scope')
        form.cleaned_data.pop('end')
        form.cleaned_data.pop('start')
        System().add_exclusion(**form.cleaned_data)
        return HttpResponseRedirect(reverse('Remember:task_edit_complex', args=[self.kwargs['task_id']]))

    def get_success_url(self):
        return reverse('Remember:index')


# connections


class ConnectionCreateView(LoginRequiredMixin, CreateView):
    form_class = ConnectionForm
    template_name = 'Remember/connection_form.html'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form = None

    def get_initial(self):
        return {
            'task_id': self.kwargs['task_id'],
            'user_select': self.kwargs['user_username']
        }

    def post(self, *args, **kwargs):
        return super().post(*args, **kwargs)

    def get(self, request, **kwargs):
        task = get_object_or_404(Task, pk=self.kwargs['task_id'])
        if task.owner != self.request.user:
            raise PermissionDenied()
        form_class = self.get_form_class()
        self.form = self.get_form(form_class)
        args = {'form': self.form,
                'users': auth_models.User.objects.all()}
        chosen_user = get_object_or_404(auth_models.User, username=kwargs['user_username'])
        self.form.fields['receiver'].queryset = System.get_public_task_connections(request.user, chosen_user,
                                                                                   self.kwargs['task_id'])
        return render(request, ConnectionCreateView.template_name, args)

    def form_valid(self, form):
        form.save(self.kwargs['task_id'])
        return HttpResponseRedirect(reverse('Remember:task_edit_complex', kwargs={'task_id': self.kwargs['task_id']}))

    def get_success_url(self):
        return HttpResponseRedirect(reverse('Remember:task_edit_complex', kwargs={'task_id': self.kwargs['task_id']}))


class ConnectionUpdateView(LoginRequiredMixin, UpdateView):
    form_class = ConnectionForm
    template_name = 'Remember/connection_update_form.html'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form = None
        self.object = None

    def form_valid(self, form):
        form.save(self.kwargs['task_id'])
        return HttpResponseRedirect(reverse('Remember:task_edit_complex', kwargs={'task_id': self.kwargs['task_id']}))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_initial(self):
        return {
            'task_id': self.kwargs['task_id']
        }

    def get(self, request, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        chosen_user = get_object_or_404(auth_models.User, username=kwargs['user_username'])
        form.fields['receiver'].queryset = System.get_public_task_connections(request.user, chosen_user,
                                                                              self.kwargs['task_id'], self.object)
        context = self.get_context_data(object=self.object, form=form, users=auth_models.User.objects.all())
        return render(request, SubConnectionUpdateView.template_name, context)

    def get_object(self, queryset=None):
        obj = get_object_or_404(TaskConnection, sender__id=self.kwargs['task_id'],
                                receiver__id=self.kwargs['receiver_id'])
        self.check_object_permissions(self.request, obj)
        return obj

    def check_object_permissions(self, request, obj):
        if obj.sender.owner != request.user and obj.sender.assigned != request.user:
            raise PermissionDenied()

    def get_success_url(self):
        return reverse('Remember:task_edit_complex', kwargs={'task_id': self.kwargs['task_id']})


# subtasks


class SubConnectionCreateView(LoginRequiredMixin, CreateView):
    form_class = SubConnectionForm
    template_name = 'Remember/subconnection_form.html'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form = None

    def get_initial(self):
        args = {
            'task_id': self.kwargs['task_id'],
            'user_select': self.kwargs['user_username']
        }
        return args

    def get(self, request, **kwargs):
        task = get_object_or_404(Task, pk=self.kwargs['task_id'])
        if task.owner != self.request.user:
            raise PermissionDenied()

        form_class = self.get_form_class()
        self.form = self.get_form(form_class)
        args = {'form': self.form,
                'users': auth_models.User.objects.all()}

        # TODO here 2
        chosen_user = get_object_or_404(auth_models.User, username=kwargs['user_username'])
        self.form.fields['receiver'].queryset = System.get_subtask_connections(request.user,
                                                                               chosen_user,
                                                                               self.kwargs['task_id'])
        return render(request, SubConnectionCreateView.template_name, args)

    def form_valid(self, form):
        form.save(self.kwargs['task_id'])
        return HttpResponseRedirect(reverse('Remember:task_edit_complex', kwargs={'task_id': self.kwargs['task_id']}))


class SubConnectionUpdateView(LoginRequiredMixin, UpdateView):
    form_class = SubConnectionForm
    template_name = 'Remember/connection_form.html'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form = None
        self.object = None

    def form_valid(self, form):
        form.save(self.kwargs['task_id'])
        return HttpResponseRedirect(reverse('Remember:task_edit_complex', kwargs={'task_id': self.kwargs['task_id']}))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_initial(self):
        return {
            'task_id': self.kwargs['task_id'],
            'user_select': self.kwargs['user_username']
        }

    def post(self, request, **kwargs):
        return super().post(request, **kwargs)

    def get(self, request, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        chosen_user = get_object_or_404(auth_models.User, username=kwargs['user_username'])
        form.fields['receiver'].queryset = System.get_subtask_connections(request.user,
                                                                          chosen_user,
                                                                          self.kwargs['task_id'])
        context = self.get_context_data(object=self.object, form=form, users=auth_models.User.objects.all())
        return render(request, SubConnectionUpdateView.template_name, context)

    def get_object(self, queryset=None):
        obj = get_object_or_404(SubtaskConnection, sender__id=self.kwargs['task_id'])
        self.check_object_permissions(self.request, obj)
        return obj

    def check_object_permissions(self, request, obj):
        if obj.sender.owner != request.user and obj.sender.assigned != request.user:
            raise PermissionDenied()

    def get_success_url(self):
        return reverse('Remember:task_edit_complex', kwargs={'task_id': self.kwargs['task_id']})


# dash


@login_required
def dashboard(request):
    if request.method == 'POST':
        if 'record_delete' in request.POST:
            Dashboard.objects.filter(pk=request.POST['record_id']).delete()
        if 'records_clear_all' in request.POST:
            Dashboard.objects.filter(user=request.user).delete()
    records = Dashboard.objects.filter(user=request.user).order_by('-datetime')
    return render(request, 'Remember/dashboard.html', {'dashboard': records})


class CalendarView(LoginRequiredMixin, TemplateView):
    template_name = 'Remember/calendar.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tasks = [task for task in
                 Task.objects.filter(Q(owner=self.request.user) | Q(assigned=self.request.user)). \
                     filter(repeat=None, repeat_of=None). \
                     exclude(end=None)]
        tasks.extend([connection.task for connection in
                      TaskRepeatConnection.objects.filter(
                          Q(repeat__template_task__owner=self.request.user)
                          | Q(repeat__template_task__assigned=self.request.user)
                      ).filter(index__lt=F('repeat__current_index') + 1)])
        context['tasks'] = tasks
        return context
