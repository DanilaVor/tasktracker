from django.contrib import admin

from .models.notification import Notification
from .models.dashboard import Dashboard
from .models.task import *
import django.contrib.auth.models as auth_models


class ChoiceInline(admin.StackedInline):
    model = Tag
    extra = 0


class CommentInline(admin.TabularInline):
    model = Comment
    extra = 0


class ConnectInline(admin.StackedInline):
    model = TaskConnection  
    extra = 0
    exclude = ['root', 'depth']
    fk_name = 'sender'


class TaskAdmin(admin.ModelAdmin):
    inlines = [ChoiceInline, CommentInline, ConnectInline]
    list_display = ('name', 'owner', 'status', 'repeat')


class ConnectionInline(admin.TabularInline):
    model = TaskRepeatConnection
    fields = ('index', 'rep_type')
    extra = 0


class RepeatAdmin(admin.ModelAdmin):
    inlines = [ConnectionInline]
    list_display = ('template_task',)


class NotificationAdmin(admin.ModelAdmin):
    list_display = ('target', 'datetime', 'text', 'is_sent')


class TagAdmin(admin.ModelAdmin):
    list_display = ('name',)


admin.site.register(Task, TaskAdmin)
admin.site.register(Notification, NotificationAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Repeat, RepeatAdmin)
admin.site.register(SubtaskConnection)
admin.site.register(TaskConnection)
admin.site.register(Dashboard)
