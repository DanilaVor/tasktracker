import datetime
import re

import django.contrib.auth.models as auth_models
from dateutil.relativedelta import relativedelta
from django import forms
from django.contrib.admin import widgets
from django.contrib.auth import authenticate
from django.forms import ModelForm
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext as _

from . import config
from . import consts
from .models.notification import Notification
from .models.task import Task, Repeat, TaskConnection, SubtaskConnection
from .system import System


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=30, initial='')
    password1 = forms.CharField(widget=forms.PasswordInput, label='Password')
    password2 = forms.CharField(widget=forms.PasswordInput, label='Repeat password')
    email = forms.EmailField(widget=forms.EmailInput)

    def clean(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        username = self.cleaned_data.get("username")
        if not re.match('[a-zA-Z0-9]+', username):
            raise forms.ValidationError(_('Username is not valid'))
        if auth_models.User.objects.filter(email=self.cleaned_data['email']).exists():
            raise forms.ValidationError(_('Email address is already registered'))
        if auth_models.User.objects.filter(username=username).exists():
            raise forms.ValidationError(_('Username address is already registered'))
        if password1 != password2:
            raise forms.ValidationError(_("Passwords doesn't match"))

    class Meta:
        model = auth_models.User


class ProfileForm(forms.Form):
    username = forms.CharField(max_length=30, initial='')
    password1 = forms.CharField(widget=forms.PasswordInput, label='Password')
    password2 = forms.CharField(widget=forms.PasswordInput, label='Repeat password')
    email = forms.EmailField(widget=forms.EmailInput)
    user = forms.ModelChoiceField(widget=forms.HiddenInput, required=False, queryset=auth_models.User.objects)

    def clean(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 != password2:
            raise forms.ValidationError(_("Passwords doesn't match"))
        if auth_models.User.objects.filter(username=self.cleaned_data['username']) \
                .exclude(pk=self.cleaned_data['user'].pk).exists():
            raise forms.ValidationError(_('Username is already registered'))
        if auth_models.User.objects.filter(email=self.cleaned_data['email']).exclude(pk=self.cleaned_data['user'].pk) \
                .exists():
            raise forms.ValidationError(_('Email address is already registered'))

    class Meta:
        model = auth_models.User


class LoginForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super().clean()
        username = cleaned_data.get("username")
        password = cleaned_data.get("password")
        user = authenticate(username=username, password=password)
        if user is None:
            raise forms.ValidationError(_('Login or password is incorrect'))
        cleaned_data['user'] = user

    class Meta:
        model = auth_models.User


# class RegistrationForm(forms.Form):
#     username = forms.EmailField(max_length=30)
#     password1 = forms.CharField(widget=forms.PasswordInput)
#     password2 = forms.CharField(widget=forms.PasswordInput)
#     first_name = forms.CharField(max_length=100)
#
#     # rest of the fields
#
#     def clean(self):
#         cleaned_data = super().clean()
#         username = cleaned_data.get("username")
#         password1 = cleaned_data.get("password1")
#         password2 = cleaned_data.get("password2")
#         if password1 != password2:
#             self.add_error('password2', _("Passwords doesn't match"))
#         # you can validate those here
#
#     class Meta:
#         model = auth_models.User


class TaskForm(ModelForm):
    __PRIORITIES = ((x, x) for x in range(config.MAX_PRIORITY + 1))
    __MAX_TAGS_LENGTH = 300

    tags = forms.CharField(max_length=__MAX_TAGS_LENGTH, required=False)
    priority = forms.ChoiceField(choices=__PRIORITIES)
    status = forms.IntegerField(min_value=consts.MIN_STATUS, max_value=consts.MAX_STATUS)
    description = forms.Textarea(attrs={'rows': 4})
    start = forms.SplitDateTimeField(widget=widgets.AdminSplitDateTime(), required=False)
    end = forms.SplitDateTimeField(widget=widgets.AdminSplitDateTime(), required=False)
    field_order = ['name', 'description', 'tags', 'priority', 'assigned']

    class Meta:
        model = Task
        exclude = ['repeat', 'id', 'owner', 'start_message_sent']
        widgets = {'description': forms.TextInput()}

    def clean_tags(self):
        tags = self.cleaned_data['tags']
        if tags is None or tags == '':
            return tags
        tags = [tag.strip() for tag in tags.split(',')]
        lengths = [len(x) for x in tags]
        if len(tags) > config.TAG_MAX_NUM:
            raise forms.ValidationError(_("Too many tags (%(num)i)"),
                                        params={'num': len(tags)})
        if min(lengths) < config.TAG_MIN_LENGTH:
            raise forms.ValidationError(_("One of tags is too short (%(len)i characters)"),
                                        params={'len': min(lengths)})
        if max(lengths) > config.TAG_MAX_LENGTH:
            raise forms.ValidationError(_("One of tags is too long (%(len)i characters)"),
                                        params={'len': max(lengths)})
        return tags

    def clean(self):
        if self.errors:
            return
        if 'end' not in self.cleaned_data:
            return
        if 'start' not in self.cleaned_data:
            return
        end = self.cleaned_data['end']
        start = self.cleaned_data['start']
        if end is None and start is not None:
            raise forms.ValidationError(_("Expected 'end' if 'start' is provided"))
        if end is not None and start is not None and end < start:
            raise forms.ValidationError(_("Expected 'end' greater than 'start'"))

    def save(self, user, **kwargs):
        task = super().save(commit=False)
        task.owner = user
        task.save()
        task.change_tags(self.cleaned_data['tags'])
        if task.result != Task.RESULT_NOT_FINISHED and task.status != consts.MAX_STATUS:
            task.status = consts.MAX_STATUS
            task.save()
        if task.status == consts.MAX_STATUS:
            if task.result == Task.RESULT_NOT_FINISHED:
                task.result = Task.DEFAULT_RESULT
                task.save()
            task.complete_task()
        if 'end' in self.cleaned_data:
            task.update_timelimit(user)
        return task


class TimelimitForm(ModelForm):
    start = forms.SplitDateTimeField(widget=widgets.AdminSplitDateTime(), required=False)
    end = forms.SplitDateTimeField(widget=widgets.AdminSplitDateTime(), required=False)

    def clean(self):
        end = self.cleaned_data['end']
        start = self.cleaned_data['start']
        if end is None and start is not None:
            raise forms.ValidationError(_("Expected 'end' if 'start' is provided"))
        if end is not None and start is not None and end < start:
            raise forms.ValidationError(_("Expected 'end' greater than 'start'"))

    def save(self, task, **kwargs):
        timelimit = super().save(commit=False)
        timelimit.task = task
        timelimit.save()
        return timelimit


class NotificationsForm(ModelForm):
    MAX_TIME_DELTAS = 100
    field_order = ['text', 'target', 'time_type', 'datetime']
    datetime = forms.SplitDateTimeField(widget=widgets.AdminSplitDateTime(), required=False)
    hours = forms.IntegerField(min_value=0, max_value=MAX_TIME_DELTAS, required=False, initial=0)
    days = forms.IntegerField(min_value=0, max_value=MAX_TIME_DELTAS, required=False, initial=0)
    weeks = forms.IntegerField(min_value=0, max_value=MAX_TIME_DELTAS, required=False, initial=0)
    task_id = forms.CharField(max_length=consts.TASK_ID_LENGTH, required=False, widget=forms.HiddenInput())

    # copy_of = forms.CharField(max_length=1, required=False, widget=forms.HiddenInput())

    class Meta:
        model = Notification
        exclude = ['task', 'delta', 'copy_of']

    def clean(self):
        time_type = self.cleaned_data['time_type']
        if time_type == Notification.RELATIVE_SHORT:
            self.add_error('time_type', "Can't create relative notification for task without timelimit")
            raise forms.ValidationError(_("Expected time limit with relative notification"))
        else:
            try:
                if self.cleaned_data['datetime'] is None:
                    self.add_error('datetime', "Expected valid datetime")
            except KeyError:
                pass
        hours = self.cleaned_data['hours']
        days = self.cleaned_data['days']
        weeks = self.cleaned_data['weeks']
        self.cleaned_data.pop('hours')
        self.cleaned_data.pop('days')
        self.cleaned_data.pop('weeks')
        self.cleaned_data['delta'] = datetime.timedelta(hours=hours, weeks=weeks, days=days)

    def save(self, task, **kwargs):
        notification = super().save(commit=False)
        notification.task = task
        if self.cleaned_data['time_type'] == consts.RELATIVE_TIME_TYPE:
            notification.datetime = task.end - self.cleaned_data['delta']
            notification.delta = self.cleaned_data['delta']
        notification.save()
        return notification


class RepeatForm(ModelForm):
    __MAX_TIME_DELTAS = 100
    __I_TH_CHOICES = ((1, '1-st'), (2, '2-nd'), (3, '3-rd'), (4, '4-th'), (5, '5-th'))
    __WEEK_DAYS = (('M', 'Monday'),
                   ('Tu', 'Tuesday'),
                   ('W', 'Wednesday'),
                   ('Th', 'Thursday'),
                   ('F', 'Friday'),
                   ('Sa', 'Saturday'),
                   ('Su', 'Sunday'))
    end = forms.SplitDateTimeField(widget=widgets.AdminSplitDateTime(), required=False)
    rule_type = forms.ChoiceField(choices=Repeat.RULE_TYPES)
    task_id = forms.CharField(max_length=consts.TASK_ID_LENGTH, widget=forms.HiddenInput(), required=False)

    rd_days = forms.IntegerField(min_value=0, max_value=__MAX_TIME_DELTAS, required=False, initial=0)
    rd_weeks = forms.IntegerField(min_value=0, max_value=__MAX_TIME_DELTAS, required=False, initial=0)
    rd_months = forms.IntegerField(min_value=0, max_value=__MAX_TIME_DELTAS, required=False, initial=0)

    every_choices = forms.ChoiceField(choices=Repeat.EVERY_CHOICES, required=False)

    i_th_i = forms.ChoiceField(choices=__I_TH_CHOICES, required=False)
    i_th_week_day = forms.ChoiceField(choices=__WEEK_DAYS, required=False)

    class Meta:
        model = Repeat
        exclude = ['template_task', 'rule_value', 'rule_type', 'current_index', 'rule_human_readable']

    def clean(self):
        if 'rule_type' in self.cleaned_data and self.cleaned_data['rule_type'] == Repeat.RELATIVE_DELTA_SHORT:
            days_valid = self.cleaned_data['rd_days'] != 0 and self.cleaned_data['rd_days'] is not None
            weeks_valid = self.cleaned_data['rd_weeks'] != 0 and self.cleaned_data['rd_weeks'] is not None
            months_valid = self.cleaned_data['rd_months'] != 0 and self.cleaned_data['rd_months'] is not None
            if not (days_valid or weeks_valid or months_valid):
                raise forms.ValidationError(_("Expected valid datetime delta with this rule type"))
            if self.cleaned_data['rd_days'] is None:
                self.cleaned_data['rd_days'] = 0
            if self.cleaned_data['rd_weeks'] is None:
                self.cleaned_data['rd_weeks'] = 0
            if self.cleaned_data['rd_months'] is None:
                self.cleaned_data['rd_months'] = 0
            self.cleaned_data['relativedelta'] = relativedelta(days=self.cleaned_data['rd_days'],
                                                               weeks=self.cleaned_data['rd_weeks'],
                                                               months=self.cleaned_data['rd_months'])


class ExclusionForm(TaskForm):
    INDEX_ID_TYPE_SHORT = 'I'
    DATE_ID_TYPE_SHORT = 'D'
    __ID_TYPES = (
        (INDEX_ID_TYPE_SHORT, 'index'),
        (DATE_ID_TYPE_SHORT, 'date')
    )
    __MAX_REPEAT_INDEX = 10000
    __EXCLUSION_TYPES = (
        ('E', 'Edited'),
        ('D', 'Deleted')
    )

    id_type = forms.ChoiceField(choices=__ID_TYPES, initial=__ID_TYPES[0])
    date = forms.DateField(widget=widgets.AdminDateWidget(), required=False)
    index = forms.IntegerField(min_value=0, max_value=__MAX_REPEAT_INDEX, initial=0, required=False)

    user = forms.CharField(max_length=config.MAX_TEXT_FIELD_LENGTH, required=False, widget=forms.HiddenInput())
    task_id = forms.CharField(max_length=consts.TASK_ID_LENGTH, required=False, widget=forms.HiddenInput())

    exclusion_type = forms.ChoiceField(choices=__EXCLUSION_TYPES)

    class Meta(TaskForm.Meta):
        widgets = {'scope': forms.HiddenInput(), 'description': forms.Textarea(attrs={'rows': 3})}

    def clean(self):
        id_type = self.cleaned_data['id_type']
        if id_type == ExclusionForm.INDEX_ID_TYPE_SHORT:
            pass
        elif id_type == ExclusionForm.DATE_ID_TYPE_SHORT:
            print(self.cleaned_data)
            if 'date' not in self.cleaned_data:
                return
            if self.cleaned_data['date'] is None:
                self.add_error('date', 'Expected date')
                return
            index = System.check_belongs(self.cleaned_data['user'],
                                         self.cleaned_data['task_id'],
                                         self.cleaned_data['date'])
            if index is None:
                self.add_error('date', _("Date doesn't belong to repeat rule"))
            self.cleaned_data['index'] = index


class ConnectionForm(ModelForm):
    task_id = forms.CharField(max_length=consts.TASK_ID_LENGTH, required=False, widget=forms.HiddenInput())
    user_select = forms.CharField(max_length=30, widget=forms.HiddenInput(), required=False)
    conn_type = forms.ChoiceField(choices=TaskConnection.TYPES)
    receiver = forms.ModelChoiceField(queryset=Task.objects, required=False)

    class Meta:
        model = TaskConnection
        exclude = ['sender']

    def save(self, sender_id, **kwargs):
        conn = super().save(commit=False)
        conn.sender = get_object_or_404(Task, pk=sender_id)
        # Task.objects.get(pk=sender_id)
        conn.receiver = self.cleaned_data['receiver']
        conn.save()
        return conn


class SubConnectionForm(ModelForm):
    task_id = forms.CharField(max_length=consts.TASK_ID_LENGTH, required=False, widget=forms.HiddenInput())
    user_select = forms.CharField(max_length=30, widget=forms.HiddenInput(), required=False)
    receiver = forms.ModelChoiceField(queryset=Task.objects)

    class Meta:
        model = SubtaskConnection
        exclude = ['sender', 'root', 'depth']

    def save(self, sender_id, **kwargs):
        conn = super().save(commit=False)
        conn.sender = get_object_or_404(Task, pk=sender_id)
        # conn.sender = Task.objects.get(pk=sender_id)
        conn.root, conn.depth = SubtaskConnection.get_root_and_depth(conn.receiver)
        conn.save()
        return conn
