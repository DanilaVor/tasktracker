# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-18 19:28
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Remember', '0033_dashboard'),
    ]

    operations = [
        migrations.RenameField(
            model_name='dashboard',
            old_name='time',
            new_name='datetime',
        ),
    ]
