from datetime import datetime

from django.core.exceptions import ValidationError
from django.db import models, IntegrityError
from django.contrib.auth import models as auth_mod
from django.db.models import Q
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from .. import config
from .. import consts
from django.utils.translation import ugettext_lazy as _
from .dashboard import Dashboard


def priority_validator(value):
    if value < 0 or value > config.MAX_PRIORITY:
        raise ValidationError(
            _('%(value)d is not a valid priority. Expected in range 0,' + str(config.MAX_PRIORITY)),
            params={'value': value},
        )


def status_validator(value):
    if value < 0 or value > consts.MAX_STATUS:
        raise ValidationError(
            _('%(value)d is not a valid status. Expected in range 0,' + str(consts.MAX_STATUS)),
            params={'value': value},
        )


class Task(models.Model):
    DEFAULT_TIMELIMIT_TEXT = 'Task {name} ({description})has hit a time limit'
    RESULT_NOT_FINISHED = 'N'
    RESULT_SUCCESS = 'S'
    RESULT_FAILED = 'F'

    DEFAULT_RESULT = RESULT_SUCCESS
    RESULT_CHOICES = (
        (RESULT_NOT_FINISHED, 'Not finished yet'),
        (RESULT_FAILED, 'Failed'),
        (RESULT_SUCCESS, 'Success')
    )
    INITIAL_RESULT = RESULT_CHOICES[0][0]
    __DB_CHOICES_LEN = 2
    SCOPE_PRIVATE_SHORT = consts.SCOPE_PRIVATE[:__DB_CHOICES_LEN]
    SCOPE_PUBLIC_SHORT = consts.SCOPE_PUBLIC[:__DB_CHOICES_LEN]
    __scope_choices = (
        (SCOPE_PRIVATE_SHORT, consts.SCOPE_PRIVATE),
        (SCOPE_PUBLIC_SHORT, consts.SCOPE_PUBLIC)
    )
    __DEFAULT_SCOPE = config.DEFAULT_SCOPE[:__DB_CHOICES_LEN]

    owner = models.ForeignKey(auth_mod.User,
                              on_delete=models.CASCADE,
                              related_name='owned_tasks')
    assigned = models.ForeignKey(auth_mod.User,
                                 on_delete=models.SET_NULL,
                                 blank=True,
                                 null=True,
                                 related_name='assigned_tasks')
    name = models.CharField(max_length=config.TASK_MAX_NAME_LENGTH)
    description = models.TextField(max_length=config.MAX_TEXT_FIELD_LENGTH,
                                   blank=True,
                                   null=True)
    priority = models.IntegerField(validators=[priority_validator])
    status = models.IntegerField(validators=[status_validator],
                                 null=True)
    result = models.CharField(max_length=config.CHOICES_FIELD_MAX_LENGTH,
                              choices=RESULT_CHOICES,
                              default=RESULT_CHOICES[0][0])
    scope = models.CharField(max_length=config.CHOICES_FIELD_MAX_LENGTH,
                             choices=__scope_choices,
                             default=__DEFAULT_SCOPE)
    repeat = models.OneToOneField('TaskRepeatConnection',
                                  on_delete=models.CASCADE,
                                  blank=True,
                                  null=True,
                                  default=None)

    start = models.DateTimeField(null=True)
    start_message_sent = models.BooleanField(default=False)
    end = models.DateTimeField(null=True)

    def change_tags(self, tags):
        for tag in tags:
            try:
                Tag.objects.create(task=self, name=tag)
            except IntegrityError:
                pass
        Tag.objects.filter(~Q(name__in=tags), task=self).delete()

    def complete_task(self):
        queue = [self]
        changed = [self]
        while queue:
            task = queue.pop()
            for connection in task.received_connections.all():
                if connection.sender not in changed and connection.sender.result == Task.RESULT_NOT_FINISHED:
                    result = connection.commit_action()
                    if result:
                        queue.append(connection.sender)
                        changed.append(connection.sender)
        queue = [self]
        while queue:
            task = queue.pop()
            for connection in task.child_connection.all():
                if connection.sender.result == Task.RESULT_NOT_FINISHED:
                    connection.sender.result = self.result
                    connection.sender.status = consts.MAX_STATUS
                    connection.sender.save()
                    queue.append(connection.sender)

    def delete_actions(self):
        queue = [self]
        while queue:
            task = queue.pop()
            for connection in task.child_connection.all():
                queue.append(connection.sender)
            task.delete()

    def end_timelimit_hit(self):
        self.status = consts.MAX_STATUS
        self.result = Task.DEFAULT_RESULT
        self.save()
        text = Task.DEFAULT_TIMELIMIT_TEXT.format(name=self.name, description=self.description)
        Dashboard.objects.create(user=self.owner, text=text, reason=Dashboard.TIME_LIMIT_REASON_SHORT)
        self.complete_task()
        if self.repeat is not None:
            self.repeat.repeat.current_index += 1
            self.repeat.repeat.save()
            self.repeat.repeat.init_next_repeat()

    def update_repeats(self):
        for task in Task.objects.filter(repeat__repeat__template_task=self,
                                        repeat__rep_type=TaskRepeatConnection.NOT_CHANGED_REPEAT_TYPE):
            task.name = self.name
            task.description = self.description
            task.scope = self.scope
            task.status = self.status
            task.result = self.result
            task.assigned = self.assigned
            task.priority = self.priority
            task.save()

    def update_timelimit(self, user):
        # check exclusions
        if not Repeat.objects.filter(template_task=self).exists():
            return
        for connection in self.repeat_of.taskrepeatconnection_set.all():
            if connection.rep_type != TaskRepeatConnection.NOT_CHANGED_REPEAT_TYPE:
                new_index = self.repeat_of.check_date_belongs(self.end, connection.task.end.date())
                if new_index is not None:
                    connection.index = new_index
                    connection.save()
                else:
                    connection.delete()
        # get required copies count
        i = 0
        import pytz
        start = self.repeat_of.template_task.end
        while start.replace(tzinfo=pytz.UTC) < datetime.utcnow().replace(tzinfo=pytz.UTC):
            start += self.repeat_of.get_next_repeat(start)
            i += 1
        # create copies
        for index in range(i):
            if not TaskRepeatConnection.objects.filter(repeat=self.repeat_of, index=index).exists():
                from ..system import System
                System.copy_task(user, self.id, self.repeat_of, index)

    def __str__(self):
        return self.name


class Comment(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    user = models.ForeignKey(auth_mod.User, on_delete=models.CASCADE)
    text = models.CharField(max_length=config.MAX_TEXT_FIELD_LENGTH)
    time = models.DateTimeField()


def tag_len_validator(value):
    if len(value) < config.TAG_MIN_LENGTH:
        raise ValidationError(
            _('%(value)d is too short. Expected in length >=' + str(config.TAG_MIN_LENGTH)),
            params={'value': value},
        )


class Tag(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    name = models.CharField(max_length=config.TAG_MAX_LENGTH, validators=[tag_len_validator])

    class Meta:
        unique_together = (("task", "name"),)


class Log(models.Model):
    EVENT_CREATED = 'C'
    EVENT_EDITED = 'E'
    LOG_EVENTS = (
        (EVENT_CREATED, 'Created'),
        (EVENT_EDITED, 'Edited')
    )

    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    event = models.CharField(max_length=config.CHOICES_FIELD_MAX_LENGTH, choices=LOG_EVENTS)
    time = models.DateTimeField()


@receiver(post_save, sender=Task)
def my_handler(sender, instance, created, **kwargs):
    if created:
        event = Log.EVENT_CREATED
    else:
        event = Log.EVENT_EDITED
    Log.objects.create(task=instance, event=event, time=datetime.now())


class SubtaskConnection(models.Model):
    # task.parent_connection.receiver.id

    sender = models.OneToOneField(Task,
                                  on_delete=models.CASCADE,
                                  related_name='parent_connection',
                                  primary_key=True)
    receiver = models.ForeignKey(Task,
                                 on_delete=models.CASCADE,
                                 related_name='child_connection')
    root = models.ForeignKey(Task,
                             on_delete=models.CASCADE,
                             related_name='root_connections',
                             null=True)
    depth = models.PositiveIntegerField(null=True)

    @staticmethod
    def get_root_and_depth(receiver):
        if SubtaskConnection.objects.filter(sender=receiver).exists():
            conn = SubtaskConnection.objects.get(sender=receiver)
            return conn.root, conn.depth + 1
        else:
            return receiver, 1


class TaskConnection(models.Model):
    TWT_TYPE_SHORT = 'TWT'
    FWT_TYPE_SHORT = 'FWT'
    TWF_TYPE_SHORT = 'TWF'
    FWF_TYPE_SHORT = 'FWF'
    INFO_TYPE_SHORT = 'INFO'

    TYPES = (
        (TWT_TYPE_SHORT, 'True when true'),
        (FWT_TYPE_SHORT, 'False when true'),
        (TWF_TYPE_SHORT, 'True when false'),
        (FWF_TYPE_SHORT, 'False when false'),
        (INFO_TYPE_SHORT, 'Info')
    )
    sender = models.ForeignKey(Task,
                               on_delete=models.CASCADE,
                               related_name='sent_connections')
    receiver = models.ForeignKey(Task,
                                 on_delete=models.CASCADE,
                                 related_name='received_connections')
    conn_type = models.CharField(max_length=config.CHOICES_FIELD_MAX_LENGTH, choices=TYPES)

    class Meta:
        unique_together = (("receiver", "sender"), )

    def commit_action(self):
        action_dict = {
            TaskConnection.TWT_TYPE_SHORT: (Task.RESULT_SUCCESS, Task.RESULT_SUCCESS),
            TaskConnection.FWT_TYPE_SHORT: (Task.RESULT_SUCCESS, Task.RESULT_FAILED),
            TaskConnection.TWF_TYPE_SHORT: (Task.RESULT_FAILED, Task.RESULT_SUCCESS),
            TaskConnection.FWF_TYPE_SHORT: (Task.RESULT_FAILED, Task.RESULT_FAILED),
        }
        if self.receiver.result == action_dict[self.conn_type][0]:
            self.sender.result = action_dict[self.conn_type][1]
            self.sender.status = consts.MAX_STATUS
            self.sender.save()
            return True
        return False


class Repeat(models.Model):
    RELATIVE_DELTA_SHORT = 'RD'
    EVERY_SHORT = 'E'
    LAST_DAY_SHORT = 'L'
    I_TH_SHORT = 'I'
    RULE_TYPES = (
        (RELATIVE_DELTA_SHORT, 'RelativeDelta'),
        (EVERY_SHORT, 'Every'),
        (LAST_DAY_SHORT, 'Last day of month'),
        (I_TH_SHORT, 'i-th day of month')
    )

    DAILY_CHOICE_SHORT = 'd'
    WORK_DAYS_CHOICE_SHORT = 'wd'
    WEEKENDS_CHOICE_SHORT = 'we'
    WEEKLY_CHOICE_SHORT = 'w'
    MONTHLY_CHOICE_SHORT = 'm'
    EVERY_CHOICES = (
        (DAILY_CHOICE_SHORT, 'day'),
        (WORK_DAYS_CHOICE_SHORT, 'work day'),
        (WEEKENDS_CHOICE_SHORT, 'week end'),
        (WEEKLY_CHOICE_SHORT, 'week'),
        (MONTHLY_CHOICE_SHORT, 'month')
    )

    template_task = models.OneToOneField(Task,
                                         on_delete=models.CASCADE,
                                         related_name='repeat_of',
                                         primary_key=True)
    end = models.DateTimeField(null=True)
    current_index = models.PositiveIntegerField(default=0)
    rule_type = models.CharField(max_length=config.CHOICES_FIELD_MAX_LENGTH,
                                 choices=RULE_TYPES,
                                 default='E')
    rule_value = models.TextField(max_length=config.MAX_TEXT_FIELD_LENGTH, blank=True)
    rule_human_readable = models.TextField(max_length=config.MAX_TEXT_FIELD_LENGTH, blank=True)

    @staticmethod
    def get_full_rule_type(short):
        for pair in Repeat.EVERY_CHOICES:
            if pair[0] == short:
                return pair[1]

    def get_next_repeat(self, date):
        from datetime import timedelta
        from dateutil.rrule import rrule
        from dateutil.rrule import MONTHLY, WEEKLY
        from dateutil.rrule import MO, TU, WE, TH, FR, SA, SU
        from dateutil.relativedelta import relativedelta
        all_days = [MO, TU, WE, TH, FR, SA, SU]
        work_days = [MO, TU, WE, TH, FR]
        weekends = [SA, SU]
        new_date = date + timedelta(days=1)

        def get_rule_weekday(short):
            week_days = {'Mo': MO, 'Tu': TU, 'We': WE, 'Th': TH, 'Fr': FR, 'Sa': SA, 'Su': SU}
            return week_days[short]

        def get_relative_delta(dict):
            return relativedelta(**{s.split('=')[0].strip(): int(s.split('=')[1]) for s in dict.split(',')})

        if self.rule_type == Repeat.EVERY_SHORT:
            if self.rule_value == Repeat.DAILY_CHOICE_SHORT:
                return relativedelta(days=1)  # rd
            if self.rule_value == Repeat.WORK_DAYS_CHOICE_SHORT:
                next_date = rrule(WEEKLY, count=1, byweekday=work_days, dtstart=new_date)[0].date()
                return next_date - date.date()
            if self.rule_value == Repeat.WEEKENDS_CHOICE_SHORT:
                next_date = rrule(WEEKLY, count=1, byweekday=weekends, dtstart=new_date)[0].date()
                return next_date - date.date()
            if self.rule_value == Repeat.WEEKLY_CHOICE_SHORT:
                return relativedelta(weeks=1)  # rd
            if self.rule_value == Repeat.MONTHLY_CHOICE_SHORT:
                return relativedelta(months=1)  # rd
        if self.rule_type == Repeat.I_TH_SHORT:
            day, index = self.rule_value.split()
            index = int(index)
            week_day = get_rule_weekday(day)
            next_date = rrule(MONTHLY, count=1, byweekday=(week_day,), bysetpos=index, dtstart=new_date)[0].date()
            return next_date - date.date()
        if self.rule_type == Repeat.LAST_DAY_SHORT:
            next_date = rrule(MONTHLY, count=1, byweekday=all_days, bysetpos=-1, dtstart=new_date)[0]
            return next_date.date() - date.date()
        if self.rule_type == Repeat.RELATIVE_DELTA_SHORT:
            return get_relative_delta(self.rule_value)

    def check_date_belongs(self, start, date):
        i = 0
        while start.date() < date:
            start += self.get_next_repeat(start.date())
            i += 1
        if start.date() == date:
            return i

    @staticmethod
    def update_repeat(task_id, index, result, status):
        if result != Task.RESULT_NOT_FINISHED:
            status = consts.MAX_STATUS
        if status == consts.MAX_STATUS and result == Task.RESULT_NOT_FINISHED:
            result = Task.DEFAULT_RESULT
        task = Task.objects.get(repeat__repeat__template_task__id=task_id,
                                repeat__index=index)
        task.result = result
        task.status = status
        task.save()

    def init_next_repeat(self):
        index = self.current_index
        while TaskRepeatConnection.objects.filter(repeat=self, rep_type=TaskRepeatConnection.DELETED_REPEAT_TYPE,
                                                  index=index).exists():
            index += 1
        from ..system import System
        System.copy_task(self.template_task.owner, self.template_task.id, self, index)


class TaskRepeatConnection(models.Model):
    NOT_CHANGED_REPEAT_TYPE = 'T'
    EDITED_REPEAT_TYPE = 'E'
    DELETED_REPEAT_TYPE = 'D'
    TYPES = (
        (NOT_CHANGED_REPEAT_TYPE, 'Template'),
        (EDITED_REPEAT_TYPE, 'Edited'),
        (DELETED_REPEAT_TYPE, 'Deleted')
    )

    repeat = models.ForeignKey(Repeat, on_delete=models.CASCADE)
    index = models.PositiveIntegerField()
    rep_type = models.CharField(max_length=config.CHOICES_FIELD_MAX_LENGTH,
                                choices=TYPES,
                                default=TYPES[0][0])
