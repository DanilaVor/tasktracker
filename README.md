## Main info

This django application was created as a task for Python course in univercity.

Full project with numerous commits you can see A [here](https://bitbucket.org/DanilaVor/remember/src), but thar repository also contains console version of the same app.

## Project info

Project is aggregation of todo list, task tracker and issue tracker.

User can create, edit tasks, assign them to other users, create notifications for tasks, repeat rules for them.

Also tasks can be linked by other tasks to create subtask connection.

Full info can be found [here](https://bitbucket.org/DanilaVor/remember/src/master/STATUS.md)
